package org.example.httpServer

import kotlinx.serialization.Serializable

@Suppress("EXPERIMENTAL_API_USAGE")
@Serializable
internal data class Fruit(val id: Int, val name: String)
