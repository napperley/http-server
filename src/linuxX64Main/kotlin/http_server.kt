@file:Suppress("EXPERIMENTAL_API_USAGE", "EXPERIMENTAL_UNSIGNED_LITERALS")

package org.example.httpServer

import kotlinx.cinterop.COpaquePointer
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.staticCFunction
import kotlinx.cinterop.toKString
import kotlinx.serialization.builtins.list
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration
import libonion.*
import kotlin.system.exitProcess

private var server: CPointer<onion>? = null

private fun homeRoute(
    @Suppress("UNUSED_PARAMETER") userData: COpaquePointer,
    @Suppress("UNUSED_PARAMETER") req: CPointer<onion_request>,
    resp: CPointer<onion_response>
): Int {
    initRuntimeIfNeeded()
    setupContentType(resp, "text/json")
    val json = Json(JsonConfiguration.Stable)
    onion_response_write0(resp, json.stringify(Fruit.serializer().list, Database.fetchAllData()))
    return OCS_PROCESSED
}

private fun helloRoute(
    @Suppress("UNUSED_PARAMETER") userData: COpaquePointer,
    req: CPointer<onion_request>,
    resp: CPointer<onion_response>
): Int {
    initRuntimeIfNeeded()
    // By default return a HTTP Bad Request.
    var result = 400
    val name = onion_request_get_query(req, "name")?.toKString() ?: ""
    if (name.isNotEmpty()) {
        setupContentType(resp, "text/plain")
        onion_response_write0(resp, "Hello $name! :)")
        result = OCS_PROCESSED
    } else {
        setupContentType(resp, "text/html")
        onion_response_write0(resp, "<h1>Bad Request (HTTP 400). The name query is missing.</h1>")
    }
    return result
}

private fun setupContentType(resp: CPointer<onion_response>, contentType: String) {
    onion_response_set_header(res = resp, key = "Content-Type", value = contentType)
}

internal fun shutdownServer(@Suppress("UNUSED_PARAMETER") signal: Int) {
    initRuntimeIfNeeded()
    println("Exiting...")
    if (server != null) {
        onion_listen_stop(server)
        Database.close()
    }
    exitProcess(-1)
}

private fun setupServer(hostname: String, port: UInt) {
    server = onion_new(O_POOL.toInt())
    onion_set_timeout(server, 5000)
    onion_set_hostname(server, hostname)
    onion_set_port(server, port.toString())
    setupRouting()
}

private fun setupRouting() {
    val urls = onion_root_url(server)
    onion_url_add(urls, "", staticCFunction(::homeRoute))
    onion_url_add(urls, "hello", staticCFunction(::helloRoute))
}

internal fun startServer() {
    val hostname = "localhost"
    val port = 8080u
    println("Starting HTTP server (on $hostname:$port)...")
    setupServer(hostname, port)
    onion_listen(server)
    println("Listening on http://$hostname:$port...")
    onion_free(server)
}