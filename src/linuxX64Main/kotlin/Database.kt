package org.example.httpServer

import kotlinx.cinterop.*
import libsqlite3.*
import platform.posix.*
import kotlin.system.exitProcess

@ThreadLocal
@Suppress("EXPERIMENTAL_API_USAGE")
internal object Database {
    private val arena = Arena()
    private val dbFilePath by lazy {
        val homeDir = getenv("HOME")?.toKString() ?: ""
        "$homeDir/fruits.db"
    }
    private var db: CPointerVar<sqlite3>? = null

    fun fetchAllData(): List<Fruit> = memScoped {
        val statement = alloc<CPointerVar<sqlite3_stmt>>()
        val result = mutableListOf<Fruit>()
        val sql = "select id, name from fruit;"
        val pzTail = alloc<CPointerVar<ByteVar>>()

        pzTail.value?.set(0, 0.toByte())
        val prepareRc = sqlite3_prepare_v2(db = db?.value, ppStmt = statement.ptr, zSql = sql, nByte = -1,
            pzTail = pzTail.ptr)
        if (prepareRc != SQLITE_OK) {
            fprintf(stderr, "Cannot fetch data: %s\n", sqlite3_errmsg(db?.value))
        } else {
            var stepRc = sqlite3_step(statement.value)
            while (stepRc == SQLITE_ROW) {
                result += Fruit(
                    id = sqlite3_column_int(statement.value, 0),
                    name = sqlite3_column_text(statement.value, 1).toKString()
                )
                stepRc = sqlite3_step(statement.value)
            }
        }
        sqlite3_finalize(statement.value)
        result
    }

    private fun CPointer<UByteVar>?.toKString() = this?.reinterpret<ByteVar>()?.toKString() ?: ""

    fun dbFileExists(): Boolean = access(dbFilePath, F_OK) != -1

    fun open() {
        db = arena.alloc()
        if (sqlite3_open("file:$dbFilePath", db?.ptr) != SQLITE_OK) {
            fprintf(stderr, "Cannot open database: %s\n", sqlite3_errmsg(db?.value))
            exitProcess(-1)
        }
    }

    fun close() {
        if (db != null) sqlite3_close(db?.value)
        db = null
    }
}
