@file:Suppress("EXPERIMENTAL_API_USAGE", "EXPERIMENTAL_UNSIGNED_LITERALS")

package org.example.httpServer

import kotlinx.cinterop.memScoped
import kotlinx.cinterop.staticCFunction
import platform.posix.*
import kotlin.system.exitProcess

fun main() = memScoped {
    signal(SIGINT, staticCFunction(::shutdownServer))
    signal(SIGTERM, staticCFunction(::shutdownServer))
    if (Database.dbFileExists()) {
        Database.open()
    } else {
        fprintf(stderr, "Database file not found.")
        exitProcess(-1)
    }
    startServer()
}
