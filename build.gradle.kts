group = "org.example"
version = "0.1-SNAPSHOT"

plugins {
    kotlin("multiplatform") version "1.3.72"
    kotlin("plugin.serialization") version "1.3.72"
}

repositories {
    jcenter()
    mavenCentral()
}

kotlin {
    linuxX64 {
        compilations.getByName("main") {
            dependencies {
                val serialisationVer = "0.20.0"
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-runtime-native:" +
                    serialisationVer)
            }
            cinterops.create("libonion") {
                includeDirs("${System.getProperty("user.home")}/libonion-0.8/include/onion")
            }
            cinterops.create("sqlite3") {
                includeDirs("/usr/include")
            }
        }
        binaries {
            executable("http_server") {
                entryPoint = "org.example.httpServer.main"
            }
        }
    }
}
